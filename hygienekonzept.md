---
title: "Hygienekonzept: Hacken Open Air 2022"
author: 
- "30.08. - 03.09.2022"
- "Pfadfinderheim Welfenhof, III Koppelweg 6, 38518 Gifhorn"
date: "Dokument vom: 19.07.2022, Version 0.2, ENTWURF"
lang: de
---

# Ziele

Unser Ziel ist es, für dich und alle anderen Teilnehmer auf dem HOA ein sicheres
Umfeld zu schaffen und möglichst allen Personen die Teilnahme zu ermöglichen.
In diesem Jahr ergibt sich für uns (wieder) die Herausforderung,
dass wir dieses sichere Umfeld im Kontext des neuen Corona-Virus (SARS-CoV-2)
ermöglichen wollen.

Dieses Hygienekonzept hat daher die folgenden Ziele:

* Verhindern bzw. Verringern des Eintragens des Virus.
* Verhindern bzw. Verringern der Ausbreitung des Virus auf der Veranstaltung.
* Erkennen von Erkrankungen und Ausbreitung des Virus auf der Veranstaltung.
* Strategien für den Fall einer erkannten Erkrankung während oder nach der
  Veranstaltung.

Auf unserer Veranstaltung werden daher die folgenden Regeln gelten:

# Antigen-Schnelltests

Wir sind uns bewusst, dass Antigen-Schnelltests Infektionen nicht
vollständig und frühzeitig erkennen können.
Für unsere Veranstaltung setzen wir trotzdem zentral auf
Antigen-Schnelltests, um Infektionen zu erkennen.

Kinder unter 6 Jahren sind von den Regelungen zu Antigen-Schnelltests ausgenommen.

Der Test darf gern auch als Selbsttest durchgeführt werden.
Du musst das Gelände also nicht verlassen um zu einem Testzentrum zu fahren.

## Vor der Anreise und bei der Ankunft

Alle Teilnehmer sollen zeitnah vor der Anreise einen Antigen-Schnelltest 
durchführen.

## Während der Veranstaltung

*Bitte beachte: Wir stellen keine Tests.*
*Bringe deine Lieblingtstests (natürlich mit entsprechender Zulassung)*
*selber mit.*

Es wird allen Teilnehmern empfohlen sich täglich mit einem
Antigen-Schnelltest zu testen.

## Umgang mit positiven Testergebnissen

Solltest Du während der Veranstaltung einen positiven Antigen-Schnelltest
haben sprich jemanden aus der Orga an und 
sondere dich von den anderen Teilnehmern ab.
Wir werden dann gemeinsam mit dir das weitere Vorgehen überlegen.

# Kontaktdatenerfassung

Es wird in diesem Jahr keine Kontaktdatenerfassung geben.
Wir empfehlen den Einsatz der CoronaWarnApp (oder alternative App).

Da das HOA im wesentlichen im Freien stattfindet, werden wir für die
Veranstaltung keinen Veranstaltungs-Code in der CWA erzeugen, sondern wollen
voll auf Bluetooth-Beacons setzen.

# Masken

Das HOA findet im Wesentlichen draußen statt.
Nehmt, nicht nur beim Tragen von Masken, Rücksicht auf einander.

In unserem einzigen Gebäude (mit Toiletten, Duschen und Lagerfläche) sollte eine FFP2-Maske
getragen werden.
Zum Duschen, sowie zum Arbeiten in der Spülküche darf diese abgenommen werden.
Kinder sind von der Maskenpflicht ausgenommen.

# Änderungsverfogung

* **Version 0.2 (2022-07-19)**: FFP2-Masken im Gebäuse sind eine Empfehlung;
  Hinweis auf Rücksichtnahme im Freie
* **Version 0.1 (2022-07-07)**: Erste Veröffentlichung zum Einsammeln von 
  Feedback

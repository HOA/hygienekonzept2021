#!/usr/bin/env bash
set -euo pipefail


pandoc hygienekonzept.md -V papersize:a4 --toc -o hygienekonzept.pdf
